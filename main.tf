terraform {
  backend "gcs" {
    bucket = "rt-terraform-bucket"
    prefix = "terraform/state"
  }
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~>4.60.1"
    }
  }
}

resource "google_container_cluster" "rt-gke-cluster" {
  name = var.cluster_name
  location = var.gcp_region
  remove_default_node_pool = true
  initial_node_count = 1
}

resource "google_container_node_pool" "rt-gke-nodes" {
  name = "${var.cluster_name}-node-pool"
  location = var.gcp_region
  cluster = google_container_cluster.rt-gke-cluster.id
  node_count = var.node_count

  node_config {
    preemptible = false
    machine_type = var.machine_type
    service_account = ""
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}