variable "gcp_project" {
  type        = string
  description = "The name of the Google Cloud Project where the cluster is to be provisioned"
}

variable "gcp_region" {
  type        = string
  default     = "europe-west6-b"
  description = "The name of the Google region where the cluster nodes are to be provisioned"
}

variable "cluster_name" {
  type        = string
  default     = "gke-terraform"
  description = "The name of the cluster to appear on the Google Cloud Console"
}

variable "machine_type" {
  type        = string
  default     = "n1-standard-2"
  description = "The name of the machine type to use for the cluster nodes"
}

variable "node_count" {
  default     = 3
  description = "The number of cluster nodes"
}