provider "google" {
  credentials = file("./rt-test-090599-6bbc1510e85b.json")
  project = var.gcp_project
  region = var.gcp_region
}